#!/usr/bin/env python3

EMOJI = "😤😱😭😴😬🙀😿🙅😦😨"
#EMOJI = "😁😂😃😇😉😈😋😍😱😀😃😄😁😆😅😂🤣☺️😊😇🙂🙃😉😌😍😘😗😙😚😋😜😝😛🤑🤗🤓😎🤡🤠😏😒😞😔😟😕🙁☹️😣😖😫😩😤😠😡😶😐😑😯😦😧😮😲😵😳😱😨😰😢😥🤤😭😓😪😴🙄🤔🤥😬🤐🤢🤧😷🤒🤕😈👿👹👺💩👻💀☠️👽👾🤖🎃😺😸😹😻😼😽🙀😿😾👐🙌👏🙏🤝👍👎👊✊🤛🤜🤞✌️🤘👌👈👉👆👇☝️✋🤚🖐🖖👋🤙💪🖕✍️🤳💅🖖💄💋👄👅👂👃👣👁👀🗣👤👥👶👦👧👨👩👱‍♀️👱👴👵👲👳‍♀️👳👮‍♀️👮👷‍♀️👷💂‍♀️💂🕵️‍♀️🕵️👩‍⚕️👨‍⚕️👩‍🌾👨‍🌾👩‍🍳👨‍🍳👩‍🎓👨‍🎓👩‍🎤👨‍🎤👩‍🏫👨‍🏫👩‍🏭👨‍🏭👩‍💻👨‍💻👩‍💼👨‍💼👩‍🔧👨‍🔧👩‍🔬👨‍🔬👩‍🎨👨‍🎨👩‍🚒👨‍🚒👩‍✈️👨‍✈️👩‍🚀👨‍🚀👩‍⚖️👨‍⚖️🤶🎅👸🤴👰🤵👼🤰🙇‍♀️🙇💁💁‍♂️🙅🙅‍♂️🙆🙆‍♂️🙋🙋‍♂️🤦‍♀️🤦‍♂️🤷‍♀️🤷‍♂️🙎🙎‍♂️🙍🙍‍♂️💇💇‍♂️💆💆‍♂️🕴💃🕺👯👯‍♂️🚶‍♀️🚶🏃‍♀️🏃👫👭👬💑👩‍❤️‍👩👨‍❤️‍👨💏👩‍❤️‍💋‍👩👨‍❤️‍💋‍👨👪👨‍👩‍👧👨‍👩‍👧‍👦👨‍👩‍👦‍👦👨‍👩‍👧‍👧👩‍👩‍👦👩‍👩‍👧👩‍👩‍👧‍👦👩‍👩‍👦‍👦👩‍👩‍👧‍👧👨‍👨‍👦👨‍👨‍👧👨‍👨‍👧‍👦👨‍👨‍👦‍👦👨‍👨‍👧‍👧👩‍👦👩‍👧👩‍👧‍👦👩‍👦‍👦👩‍👧‍👧👨‍👦👨‍👧👨‍👧‍👦👨‍👦‍👦👨‍👧‍👧👚👕👖👔👗👙👘👠👡👢👞👟👒🎩🎓👑⛑🎒👝👛👜💼👓🕶🌂☂️"

import random

random.seed(2)
CNT = 150
variants = ["".join(random.sample(EMOJI, 5)) for _ in range(CNT)]
assert len(set(variants)) == len(variants), len(set(variants))

print(open('header.tex').read())

for seed in variants:
    random.seed(seed)
    t = open('template.tex').read()
    r = lambda a, b: random.randrange(a, b)
    cons = lambda s, e, m: (s << 11) | (e << 6) | m
    def gen_float12_pair():
        exp1 = r(10, 20)
        exp2 = exp1 + r(-3, 3)
        f1 = cons(r(0, 2), exp1, r(0, 2**6))
        f2 = cons(r(0, 2), exp2, r(0, 2**6))
        return (f1, f2)
    s1, s2 = gen_float12_pair()
    p1, p2 = gen_float12_pair()
    replaces = {
        r'\SEED': seed,
        r'\TENTOTWO': str(r(200, 500)),
        r'\TENTOHEX': str(r(200, 500)),
        r'\TWOTOTEN': bin(r(200, 500))[2:],
        r'\TWOTOHEX': bin(r(1000000, 2000000))[2:],
        r'\CODENEG': str(r(-126, 0)),
        r'\CODEPOS': str(r(1, 127)),
        r'\ABS1': hex(r(0, 2**15)),
        r'\ABS2': hex(r(2**15, 2**16)),
        r'\FLOATSMALL': str(r(1, 31)),
        r'\FLOATMID': str(r(200, 500)),
        r'\FLOATBIG': str(r(10**6, 2 * 10**6)),
        r'\FINDX': str(2 ** r(1, 6)),
        r'\SUM1': hex(s1),
        r'\SUM2': hex(s2),
        r'\PROD1': hex(p1),
        r'\PROD2': hex(p2)
    }
    for var, val in replaces.items():
        t = t.replace(var, val)
    print(t)

print(open('footer.tex').read())
    
