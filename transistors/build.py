#!/usr/bin/env python3

import random
import code128

CNT = 150

print(open('header.tex').read())
shares = open('shares.txt').readlines()

PREFIX = "goo.gl/hiQjCL "

for seed in range(CNT):
    text = PREFIX + shares[seed].strip()
    code128.image(text, height=200, thickness=4).save('pics/barcode{}.png'.format(seed))
    random.seed(seed)
    t = open('template.tex').read()
    r = lambda a, b: random.randrange(a, b)
    cons = lambda s, e, m: (s << 11) | (e << 6) | m
    def gen_float12_pair():
        exp1 = r(10, 20)
        exp2 = exp1 + r(-3, 3)
        f1 = cons(r(0, 2), exp1, r(0, 2**6))
        f2 = cons(r(0, 2), exp2, r(0, 2**6))
        return (f1, f2)
    s1, s2 = gen_float12_pair()
    p1, p2 = gen_float12_pair()
    replaces = {
        r'\I': str(seed),
        r'\GOODSCHEME': random.choice(['nand3.tex', 'nor3.tex']),
        r'\BADSCHEME': random.choice(['bad-00x.tex', 'bad-11z.tex']),
        r'\FUN': random.choice([
            'медианы трёх (1, если на $\ge 2$ входах 1)',
            'меньшинства трёх (возвращает бит, которого меньше на входах)'])
    }
    for j in range(1, 5):
        V = random.choice([3.3, 5.0])
        VOL, VIL, VIH, VOH = sorted([random.random() * V for _ in range(4)])
        replaces[r'\V%d'%j] = '%0.1f'%V
        replaces[r'\VOL%d'%j] = '%0.2f'%VOL
        replaces[r'\VIL%d'%j] = '%0.2f'%VIL
        replaces[r'\VIH%d'%j] = '%0.2f'%VIH
        replaces[r'\VOH%d'%j] = '%0.2f'%VOH

    GATES = [
        "<<И>> и <<НЕ>>",
        "<<ИЛИ>> и <<НЕ>>",
        "<<И-НЕ>> и <<И>>",
        "<<ИЛИ-НЕ>> и <<И>>",
    ]
    replaces[r'\GATES'] = random.choice(GATES)

    vals = []
    while True:
        vals = [random.randrange(2) for _ in range(8)]
        if 3 <= sum(vals) <= 5:
            break

    for j in range(8):
        replaces[r'\VAL%d'%(j+1)] = str(vals[j])
        
    for var, val in replaces.items():
        t = t.replace(var, val)
    print(t)

print(open('footer.tex').read())
    
